FROM node:22 AS build
WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY  . .
RUN npm run build
FROM node:22
WORKDIR /app
COPY --from=build /app/dist ./dist
COPY ./package*.json ./
RUN npm install --only=production
EXPOSE 3000
CMD ["node", "dist/main.js"]
