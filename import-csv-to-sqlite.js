const fs = require('fs');
const csv = require('csv-parser');
const sqlite3 = require('sqlite3').verbose();

// เปิดการเชื่อมต่อกับ SQLite database
let db = new sqlite3.Database('C:/Dev_Nest/nlp_project_backed/metal.sqlite');

// อ่านข้อมูลจากทุกไฟล์ CSV ในไดเร็กทอรี 'CSV' และเพิ่มข้อมูลลงในตาราง "post" ใน SQLite
fs.readdir('C:/Dev_Nest/DB-NLP/CSV', (err, files) => {
  if (err) {
    console.error("Error reading directory:", err);
    return;
  }
  
  files.forEach(file => {
    if (file.endsWith('.csv')) {
      fs.createReadStream(`C:/Dev_Nest/DB-NLP/CSV/${file}`)
        .pipe(csv())
        .on('data', (row) => {
          // เพิ่มข้อมูลลงในตาราง "post"
          db.run(`INSERT INTO post (title, txtContent, source) VALUES (?, ?, ?)`, [row['หัวข้อ'], row['เนื้อหา'], row['web-scraper-start-url']], function(err) {
            if (err) {
              return console.error(err.message);
            }
            console.log(`A row has been inserted with rowid ${this.lastID}`);
          });
        })
        .on('end', () => {
          console.log(`CSV file ${file} successfully processed`);
        });
    }
  });
});
