const fs = require('fs');

const sourceFolder = 'C:/Dev_Nest/DB-NLP/CSV';
const cleanFolder = 'C:/Dev_Nest/DB-NLP/clean';

// อ่านไฟล์ทั้งสองโฟลเดอร์
const sourceFiles = fs.readdirSync(sourceFolder);
const cleanFiles = fs.readdirSync(cleanFolder);

// หาไฟล์ที่ถูกข้าม
const skippedFiles = sourceFiles.filter(file => !cleanFiles.includes(file));

// แสดงรายชื่อไฟล์ที่ถูกข้าม
console.log("Skipped files:", skippedFiles);
