const fs = require('fs');
const csv = require('csv-parser');
const path = require('path');

// กำหนดโฟลเดอร์ที่มีไฟล์ CSV เริ่มต้นและโฟลเดอร์ที่ต้องการบันทึกไฟล์ที่ clean แล้ว
const csvFolder = 'C:/Dev_Nest/DB-NLP/CSV';
const cleanFolder = 'C:/Dev_Nest/DB-NLP/clean';

// ตรวจสอบว่าโฟลเดอร์ที่ต้องการบันทึกไฟล์ที่ clean แล้วมีอยู่หรือไม่
if (!fs.existsSync(cleanFolder)) {
  fs.mkdirSync(cleanFolder, { recursive: true });
}

// วนลูปผ่านไฟล์ CSV ในโฟลเดอร์ csvFolder
fs.readdir(csvFolder, (err, files) => {
  if (err) {
    console.error("Error reading directory:", err);
    return;
  }
  
  files.forEach(file => {
    if (file.endsWith('.csv')) {
      const inputFilePath = path.join(csvFolder, file);
      const outputFilePath = path.join(cleanFolder, file);

      fs.createReadStream(inputFilePath)
        .pipe(csv())
        .on('data', (row) => {
          // เลือกฟิลด์ที่ต้องการ clean
          const cleanedData = {
            'web-scraper-start-url': row['web-scraper-start-url'],
            'หัวข้อ': row['หัวข้อ'],
            'เนื้อหา': row['เนื้อหา']
          };

          // เขียนข้อมูลที่ clean แล้วลงในไฟล์ใหม่
          fs.appendFileSync(outputFilePath, Object.values(cleanedData).join(',') + '\n');
        })
        .on('end', () => {
          console.log(`File ${file} cleaned successfully.`);
        });
    }
  });
});
