import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Roles } from 'src/auths/roles.decorator';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';
import { UserRole } from './entities/user.entity';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }
  @Get('count-roles')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  async countRoles() {
    const counts = await this.usersService.countUserRoles();
    return counts;
  }
  @Get('volunteer-post-counts')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  async getVolunteerPostCounts() {
    const result = await this.usersService.getVolunteerPostCounts();
    return result;
  }
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
