import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { ReportPost } from 'src/report-post/entities/report-post.entity';
import { AuthsModule } from 'src/auths/auths.module';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';

@Module({
  imports: [TypeOrmModule.forFeature([User, ReportPost]), AuthsModule],
  controllers: [UsersController],
  providers: [UsersService, JwtRoleGuard],
})
export class UsersModule {}
