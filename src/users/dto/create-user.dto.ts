import { IsEnum } from 'class-validator';
import { UserRole } from '../entities/user.entity';

export class CreateUserDto {
  username: string;
  email: string;
  password: string;
  @IsEnum(UserRole)
  role: UserRole;
}
