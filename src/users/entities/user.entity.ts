import { PostTag } from 'src/post-tags/entities/post-tag.entity';
import { Post } from 'src/posts/entities/post.entity';
import { ReportPost } from 'src/report-post/entities/report-post.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
export enum UserRole {
  ADMIN = 'admin',
  PERSONNEL = 'personnel',
  RESEARCHER = 'researcher',
  VOLUNTEER = 'volunteer',
}
@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.PERSONNEL, // กำหนดค่า default เป็น 'volunteer'
  })
  role: UserRole;

  @OneToMany(() => Post, (post) => post.user)
  post: Post[];

  @OneToMany(() => PostTag, (postTag) => postTag.user)
  postTags: PostTag[];

  @OneToMany(() => ReportPost, (reportPost) => reportPost.reportedBy)
  reportedPosts: ReportPost[];
}
