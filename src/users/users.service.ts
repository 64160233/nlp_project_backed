import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserRole } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto) {
    const user = this.userRepository.save(createUserDto);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }
  async getVolunteerPostCounts() {
    return this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.post', 'post')
      .select(['user.id', 'user.username'])
      .addSelect('COUNT(post.id)', 'postCount')
      .where('user.role = :role', { role: 'volunteer' })
      .groupBy('user.id')
      .getRawMany();
  }
  async countUserRoles() {
    // Exclude 'admin' from the count
    const userRoles = await this.userRepository
      .createQueryBuilder('user')
      .select('user.role')
      .addSelect('COUNT(user.id)', 'count')
      .where('user.role != :adminRole', { adminRole: UserRole.ADMIN })
      .groupBy('user.role')
      .getRawMany();

    return userRoles;
  }
  async findOneByUsername(username: string): Promise<User | undefined> {
    return this.userRepository.findOne({ where: { username } });
  }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ where: { email } });
  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
