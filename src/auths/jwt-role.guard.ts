import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';
import { UserRole } from 'src/users/entities/user.entity';

@Injectable()
export class JwtRoleGuard implements CanActivate {
  constructor(private reflector: Reflector, private jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const requiredRoles = this.reflector.get<UserRole[]>(
      'roles',
      context.getHandler(),
    );
    if (!requiredRoles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers['authorization'];

    if (!authHeader) {
      console.log('No Authorization header');
      return false;
    }

    const token = authHeader.split(' ')[1];
    const payload = this.jwtService.decode(token);

    console.log('Decoded payload:', payload);

    if (!payload || !payload['role']) {
      console.log('No role in payload');
      return false;
    }

    const userRole = payload['role'];
    console.log('User role:', userRole);

    return requiredRoles.includes(userRole);
  }
}
