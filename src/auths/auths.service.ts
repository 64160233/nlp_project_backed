import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { User } from 'src/users/entities/user.entity';
import { UserRole } from 'src/users/entities/user.entity';
import * as bcrypt from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthsService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  findAll() {
    return `This action returns all auths`;
  }

  findOne(id: number) {
    return `This action returns a #${id} auth`;
  }
  remove(id: number) {
    return `This action removes a #${id} auth`;
  }
  async register(userDto: CreateUserDto): Promise<User> {
    try {
      const existingUser = await this.usersService.findOneByEmail(
        userDto.email,
      );
      if (existingUser) {
        throw new HttpException(
          'Email already in use.',
          HttpStatus.BAD_REQUEST,
        );
      }

      const passwordRegex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#.#])[A-Za-z\d@$!%*?&#.#]{8,12}$/;
      if (!passwordRegex.test(userDto.password)) {
        throw new HttpException(
          'Password does not meet the requirements.',
          HttpStatus.BAD_REQUEST,
        );
      }

      userDto.role = UserRole.PERSONNEL;
      const user = this.usersRepository.create({
        username: userDto.username,
        email: userDto.email,
        password: userDto.password,
        role: userDto.role,
      });

      const savedUser = await this.usersRepository.save(user);
      return savedUser; // ส่งกลับข้อมูลผู้ใช้ที่ถูกบันทึก
    } catch (error) {
      console.error('Registration error:', error);
      if (error instanceof HttpException) {
        throw error;
      }
      throw new HttpException(
        'An error occurred while registering the user.',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async registerResearcher(userDto: CreateUserDto): Promise<User> {
    try {
      // Check if email already exists
      const existingUser = await this.usersService.findOneByEmail(
        userDto.email,
      );

      if (existingUser) {
        throw new HttpException(
          'Email already in use.',
          HttpStatus.BAD_REQUEST,
        );
      }

      // Validate password
      const passwordRegex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#.#])[A-Za-z\d@$!%*?&#.#]{8,12}$/;
      if (!passwordRegex.test(userDto.password)) {
        throw new HttpException(
          'Password does not meet the requirements.',
          HttpStatus.BAD_REQUEST,
        );
      }

      // Set user role to "researcher"
      userDto.role = UserRole.RESEARCHER;

      // Create user entity
      const user = this.usersRepository.create({
        username: userDto.username,
        email: userDto.email,
        password: userDto.password,
        role: userDto.role,
      });

      // Save user to database
      const savedUser = await this.usersRepository.save(user);

      // Return saved user
      return savedUser;
    } catch (error) {
      console.error(error);
      throw new HttpException(error.message, HttpStatus.NOT_FOUND);
    }
  }
  async registerVolunteer(userDto: CreateUserDto): Promise<User> {
    try {
      // Check if email already exists
      const existingUser = await this.usersService.findOneByEmail(
        userDto.email,
      );

      if (existingUser) {
        throw new HttpException(
          'Email already in use.',
          HttpStatus.BAD_REQUEST,
        );
      }

      // Validate password
      const passwordRegex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#.#])[A-Za-z\d@$!%*?&#.#]{8,12}$/;
      if (!passwordRegex.test(userDto.password)) {
        throw new HttpException(
          'Password does not meet the requirements.',
          HttpStatus.BAD_REQUEST,
        );
      }

      userDto.role = UserRole.VOLUNTEER;

      // Create user entity
      const user = this.usersRepository.create({
        username: userDto.username,
        email: userDto.email,
        password: userDto.password,
        role: userDto.role,
      });

      // Save user to database
      const savedUser = await this.usersRepository.save(user);

      // Return saved user
      return savedUser;
    } catch (error) {
      console.error(error);
      throw new HttpException(error.message, HttpStatus.NOT_FOUND);
    }
  }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (password !== user.password) {
      throw new NotFoundException('Invalid credentials');
    }

    const { password: userPassword, ...result } = user;

    const payload = { username: result.username, role: result.role };
    const token = this.jwtService.sign(payload);

    return {
      user: result,
      access_token: token,
    };
  }

  private async comparePasswords(
    plainPassword: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(plainPassword, hashedPassword);
  }
}
