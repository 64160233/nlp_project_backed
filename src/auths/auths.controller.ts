import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { AuthsService } from './auths.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

@Controller('auth')
export class AuthsController {
  constructor(private readonly authsService: AuthsService) {}

  @Post('register')
  async register(@Body() userDto: CreateUserDto) {
    console.log('เริ่มต้นสมัครสมาชิก');
    return await this.authsService.register(userDto);
  }
  @Post('register-Researcher')
  async registerResearcher(@Body() createUserDto: CreateUserDto) {
    return this.authsService.registerResearcher(createUserDto);
  }
  @Post('register-Volunteer')
  async registerVolunteer(@Body() createUserDto: CreateUserDto) {
    return this.authsService.registerVolunteer(createUserDto);
  }
  @Post('login')
  async login(@Body() createUserDto: CreateUserDto) {
    return this.authsService.validateUser(
      createUserDto.email,
      createUserDto.password,
    );
  }

  @Get()
  findAll() {
    return 'ทดสอบ';
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.authsService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.authsService.remove(+id);
  }
}
