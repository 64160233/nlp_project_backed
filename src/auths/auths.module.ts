import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtRoleGuard } from './jwt-role.guard';
import { AuthsService } from './auths.service';
import { UsersService } from 'src/users/users.service';
import { AuthsController } from './auths.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET_KEY'),
        signOptions: { expiresIn: '60m' },
      }),
    }),
    ConfigModule.forRoot(),
  ],
  controllers: [AuthsController],
  providers: [AuthsService, UsersService, JwtRoleGuard],
  exports: [JwtModule],
})
export class AuthsModule {}
