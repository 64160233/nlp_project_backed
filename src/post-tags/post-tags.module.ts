import { Module } from '@nestjs/common';
import { PostTagsService } from './post-tags.service';
import { PostTagsController } from './post-tags.controller';
import { PostTag } from './entities/post-tag.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from 'src/posts/entities/post.entity';
import { User } from 'src/users/entities/user.entity';
import { Tag } from 'src/tags/entities/tag.entity';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';
import { AuthsModule } from 'src/auths/auths.module';

@Module({
  imports: [TypeOrmModule.forFeature([Post, PostTag, User, Tag]), AuthsModule],
  controllers: [PostTagsController],
  providers: [PostTagsService, JwtRoleGuard],
})
export class PostTagsModule {}
