export class CreatePostTagDto {
  timestamp: Date;
  userId: number;
  postId: number;
  tagId: number;
}
