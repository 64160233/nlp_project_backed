import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePostTagDto } from './dto/create-post-tag.dto';
import { UpdatePostTagDto } from './dto/update-post-tag.dto';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { PostTag } from './entities/post-tag.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class PostTagsService {
  constructor(
    @InjectRepository(PostTag)
    private postTagRepository: Repository<PostTag>,
    @InjectDataSource()
    private dataSource: DataSource,
  ) {}

  async SearchTextAndUserId(searchText: string, userId: number) {
    return this.postTagRepository
      .createQueryBuilder('pt')
      .innerJoinAndSelect('pt.post', 'p')
      .innerJoinAndSelect('pt.tag', 't')
      .innerJoinAndSelect('pt.user', 'u')
      .where(
        '(p.title LIKE :searchText OR p.txtContent LIKE :searchText Or t.tagName LIKE :searchText)',
        {
          searchText: `%${searchText}%`,
        },
      )
      .andWhere('pt.user_id = :userId', { userId })
      .orderBy('p.id', 'DESC')
      .getMany();
  }

  create(createPostTagDto: CreatePostTagDto) {
    return this.postTagRepository.save(createPostTagDto);
  }

  async findAll(): Promise<PostTag[]> {
    return this.postTagRepository.find({
      relations: ['user', 'post', 'tag'],
    });
  }
  async countUniqueTaggedPosts(): Promise<number> {
    const result = await this.postTagRepository
      .createQueryBuilder('postTag')
      .select('COUNT(DISTINCT postTag.post_id)', 'count')
      .getRawOne();

    return parseInt(result.count, 10);
  }

  // Method to count posts tagged with tag_id = 2
  async countPostsWithSpecificTag(tagId = 2): Promise<number> {
    const result = await this.postTagRepository
      .createQueryBuilder('postTag')
      .where('postTag.tag_id = :tagId', { tagId })
      .select('COUNT(DISTINCT postTag.post_id)', 'count')
      .getRawOne();

    return parseInt(result.count, 10);
  }
  async findAllByUserId(userId: number): Promise<PostTag[]> {
    const postTags = await this.postTagRepository.find({
      where: { user: { id: userId } },
      relations: ['user', 'post', 'tag'],
    });

    if (!postTags || postTags.length === 0) {
      throw new NotFoundException('PostTags not found for the given user');
    }

    return postTags;
  }

  async findOne(id: number) {
    const postTag = await this.postTagRepository.findOne({
      where: { id: id },
      relations: ['user', 'post', 'tag'],
    });
    if (!postTag) {
      throw new NotFoundException('postTag not found');
    } else {
      return postTag;
    }
  }

  async update(id: number, updatePostTagDto: UpdatePostTagDto) {
    const posttag = await this.postTagRepository.findOneBy({ id: id });
    if (!posttag) {
      throw new NotFoundException();
    }
    const updatedPostTag = { ...posttag, ...updatePostTagDto };
    return this.postTagRepository.save(updatedPostTag);
  }

  remove(id: number) {
    return `This action removes a #${id} postTag`;
  }
}
