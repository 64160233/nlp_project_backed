import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { PostTagsService } from './post-tags.service';
import { CreatePostTagDto } from './dto/create-post-tag.dto';
import { UpdatePostTagDto } from './dto/update-post-tag.dto';
import { PostTag } from './entities/post-tag.entity';
import { Roles } from 'src/auths/roles.decorator';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';
import { UserRole } from 'src/users/entities/user.entity';

@Controller('post-tags')
export class PostTagsController {
  constructor(private readonly postTagsService: PostTagsService) {}

  @Post()
  async create(@Body() createPostTagDto: CreatePostTagDto) {
    return this.postTagsService.create(createPostTagDto);
  }
  @Get('search')
  fineAllPost(@Query() query: { searchText?: any; userId?: any }) {
    console.log('Query Parameters:', query);
    if (query.userId && query.searchText) {
      console.log('Search Text:', query.searchText);
      return this.postTagsService.SearchTextAndUserId(
        query.searchText,
        query.userId,
      );
    } else {
      return this.postTagsService.findAll();
    }
  }

  @Get()
  findAll() {
    return this.postTagsService.findAll();
  }
  @Get('count-unique-tagged-posts')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  async countUniqueTaggedPosts(): Promise<number> {
    return this.postTagsService.countUniqueTaggedPosts();
  }

  @Get('count-posts-with-tag/:tagId')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  async countPostsWithSpecificTag(
    @Param('tagId') tagId: number,
  ): Promise<number> {
    return this.postTagsService.countPostsWithSpecificTag(tagId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postTagsService.findOne(+id);
  }

  @Get('user/:userId')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.PERSONNEL, UserRole.RESEARCHER)
  async findAllByUserId(
    @Param('userId', ParseIntPipe) userId: number,
  ): Promise<PostTag[]> {
    try {
      const postTags = await this.postTagsService.findAllByUserId(userId);
      return postTags;
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException('PostTags not found for the given user');
      }
      throw error;
    }
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePostTagDto: UpdatePostTagDto) {
    return this.postTagsService.update(+id, updatePostTagDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.postTagsService.remove(+id);
  }
}
