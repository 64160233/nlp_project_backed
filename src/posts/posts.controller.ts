import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';
import { Roles } from 'src/auths/roles.decorator';
import { UserRole } from 'src/users/entities/user.entity';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.VOLUNTEER)
  create(@Body() createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }
  @Get('exclude-tags/:user_id')
  @UseGuards(JwtRoleGuard)
  @Roles(
    UserRole.PERSONNEL,
    UserRole.RESEARCHER,
    UserRole.ADMIN,
    UserRole.VOLUNTEER,
  )
  async findAllInTag(@Param('user_id') user_id: number) {
    return this.postsService.findAllInTag(user_id);
  }

  @Get()
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.PERSONNEL, UserRole.RESEARCHER, UserRole.ADMIN)
  findAll() {
    return this.postsService.findAll();
  }

  @Get('by-source/:user_id')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.VOLUNTEER, UserRole.ADMIN)
  fineAllBySource(
    @Param('user_id') user_id: string,
    @Query() query: { searchText?: string; searchDate?: string },
  ) {
    if (query.searchDate && query.searchText) {
      console.log(query);
      return this.postsService.getPostDiaryBySearchTextAndDate(
        query.searchText,
        query.searchDate,
        +user_id,
      );
    } else if (query.searchText) {
      return this.postsService.getPostDiaryBySearchText(
        query.searchText,
        +user_id,
      );
    } else if (query.searchDate) {
      console.log(query);
      return this.postsService.getPostDiaryBySearchDate(
        query.searchDate,
        +user_id,
      );
    }
    console.log(query);
    return this.postsService.findAllBySource(+user_id);
  }
  @Get('/search')
  async searchPostsByDate(
    @Query('searchDate') searchDate: string,
    @Query('userId') userId: number,
  ) {
    try {
      const posts = await this.postsService.getPostDiaryBySearchDate(
        searchDate,
        userId,
      );
      return {
        status: HttpStatus.OK,
        message: 'Posts retrieved successfully',
        data: posts,
      };
    } catch (error) {
      return {
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        message: 'Failed to retrieve posts',
        error: error.message,
      };
    }
  }

  @Get('by-source/:user_id/:id')
  fineOneBySourceId(
    @Param('user_id') user_id: string,
    @Param('id') id: string,
  ) {
    return this.postsService.findOneBySource(+user_id, +id);
  }

  @Get('searchAll')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  fineAllPost(@Query() query: { searchText?: any }) {
    console.log('Query Parameters:', query);
    if (query.searchText) {
      console.log('Search Text:', query.searchText);
      return this.postsService.searchPostsByText(query.searchText);
    } else {
      return this.postsService.findAllPost();
    }
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.postsService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.VOLUNTEER)
  update(@Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postsService.update(+id, updatePostDto);
  }

  @Delete(':id')
  @UseGuards(JwtRoleGuard)
  @Roles(UserRole.ADMIN)
  remove(@Param('id') id: string) {
    return this.postsService.remove(+id);
  }
}
