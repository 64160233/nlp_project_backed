export class CreatePostDto {
  content: string;
  source: string;
  datetime: string; // ใช้ string สำหรับ input datetime
  user_id: number;
}
