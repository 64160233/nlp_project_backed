import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { Post } from './entities/post.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { PostTag } from 'src/post-tags/entities/post-tag.entity';
import { ReportPost } from 'src/report-post/entities/report-post.entity';
import { AuthsModule } from '../auths/auths.module';
import { JwtRoleGuard } from 'src/auths/jwt-role.guard';

@Module({
  imports: [
    TypeOrmModule.forFeature([Post, User, PostTag, ReportPost]),
    AuthsModule,
  ],
  controllers: [PostsController],
  providers: [PostsService, JwtRoleGuard],
})
export class PostsModule {}
