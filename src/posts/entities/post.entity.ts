import { PostTag } from 'src/post-tags/entities/post-tag.entity';
import { ReportPost } from 'src/report-post/entities/report-post.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  title: string | null;

  @Column({ type: 'varchar', length: 5000 })
  txtContent: string;

  @Column({ type: 'text', nullable: true })
  datetime: string;

  @Column()
  source: string;

  @ManyToOne(() => User, (user) => user.post)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @OneToMany(() => PostTag, (postTag) => postTag.post)
  postTags: PostTag[];

  @DeleteDateColumn({ name: 'deletedAt' })
  deletedAt: Date;

  getPostTags(): PostTag[] {
    return this.postTags;
  }
  @OneToMany(() => ReportPost, (reportPost) => reportPost.post)
  reports: ReportPost[];
}
