import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from './entities/post.entity';
import { User } from 'src/users/entities/user.entity';
import { Equal, Repository, Not, In } from 'typeorm';
import * as moment from 'moment-timezone';
import { format } from 'date-fns';
import { PostTag } from 'src/post-tags/entities/post-tag.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post)
    private postsRepository: Repository<Post>,
    @InjectRepository(User)
    private usersRepository: Repository<User>, // เพิ่ม User repository
    @InjectRepository(PostTag)
    private postTagRepository: Repository<PostTag>,
  ) {}

  async create(createPostDto: CreatePostDto) {
    const { user_id, datetime, ...postDetails } = createPostDto;
    console.log('Original datetime:', datetime);
    const user = await this.usersRepository.findOneBy({ id: user_id });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    const convertedDatetime = moment
      .tz(datetime, 'Asia/Bangkok')
      .format('YYYY-MM-DD HH:mm:ss');
    console.log('Converted datetime:', convertedDatetime);

    const post = this.postsRepository.create({
      ...postDetails,
      datetime: convertedDatetime,
      user, // เชื่อมโยง User กับ Post
    });

    return this.postsRepository.save(post);
  }

  findAll() {
    return this.postsRepository.find({
      relations: ['user'],
    });
  }
  findAllPost() {
    return this.postsRepository.find();
  }

  async findAllInTag(user_id: number) {
    const postTags = await this.postTagRepository.find({
      where: { user: { id: user_id } },
      relations: ['post'],
    });

    const postIds = postTags.map((postTag) => postTag.post.id);

    return this.postsRepository.find({
      where: { id: Not(In(postIds)) },
      relations: ['user'],
    });
  }

  async findAllBySource(user_id: number) {
    return this.postsRepository.find({
      where: { source: 'Diary', user: Equal(user_id) },
      relations: ['user'],
      order: {
        id: 'DESC',
      },
    });
  }

  async getPostDiaryBySearchText(searchText: string, user_id: number) {
    return this.postsRepository
      .createQueryBuilder('post')
      .leftJoinAndSelect('post.user', 'user')
      .where('post.txtContent LIKE :searchText', {
        searchText: `%${searchText}%`,
      })
      .andWhere('post.user_id = :user_id', { user_id })
      .orderBy('post.id', 'DESC') // เรียงจากมากไปน้อยตามจำนวนตัวอักษรที่ตรงกับ searchText
      .getMany();
  }
  async getPostDiaryBySearchDate(SearchDate: string, user_id: number) {
    const formattedDate = format(new Date(SearchDate), 'yyyy-MM-dd');

    return this.postsRepository
      .createQueryBuilder('post')
      .leftJoinAndSelect('post.user', 'user')
      .where('DATE(post.datetime) = :SearchDate', { SearchDate: formattedDate })
      .andWhere('post.user_id = :user_id', { user_id })
      .orderBy('post.id', 'DESC')
      .getMany();
  }

  async getPostDiaryBySearchTextAndDate(
    searchText: string,
    searchDate: string,
    user_id: number,
  ) {
    const formattedDate = format(new Date(searchDate), 'yyyy-MM-dd');
    return this.postsRepository
      .createQueryBuilder('post')
      .leftJoinAndSelect('post.user', 'user')
      .where('post.txtContent LIKE :searchText', {
        searchText: `%${searchText}%`,
      })
      .andWhere('post.user_id = :user_id', { user_id })
      .andWhere('DATE(post.datetime) = :searchDate', {
        searchDate: formattedDate,
      })
      .orderBy('post.id', 'DESC') // เรียงจากมากไปน้อยตามจำนวนตัวอักษรที่ตรงกับ searchText
      .getMany();
  }

  findOneBySource(user_id: number, id: number) {
    const post = this.postsRepository.findOne({
      where: { source: 'Diary', id: id, user: Equal(user_id) },
      relations: ['user'],
    });
    if (!post) {
      throw new NotFoundException('diary not found');
    } else {
      return post;
    }
  }

  async searchPostsByText(searchText: string) {
    return this.postsRepository
      .createQueryBuilder('post')
      .where('post.txtContent LIKE :searchText', {
        searchText: `%${searchText}%`,
      })
      .orWhere('post.title LIKE :searchText', {
        searchText: `%${searchText}%`,
      })
      .orderBy('post.id', 'DESC')
      .getMany();
  }

  async findOne(id: number) {
    const post = await this.postsRepository.findOne({
      where: { id: id },
      relations: ['user'],
    });
    if (!post) {
      throw new NotFoundException('post not found');
    } else {
      return post;
    }
  }

  async update(id: number, updatePostDto: UpdatePostDto) {
    const post = await this.postsRepository.findOneBy({ id: id });
    if (!post) {
      throw new NotFoundException();
    }
    const updatePost = { ...post, ...updatePostDto };
    return this.postsRepository.save(updatePost);
  }

  async remove(id: number) {
    const result = await this.postsRepository.softDelete(id);
    if (result.affected === 0) {
      throw new HttpException('Cashier not found', HttpStatus.NOT_FOUND);
    }
  }
}
