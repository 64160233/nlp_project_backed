import { PostTag } from 'src/post-tags/entities/post-tag.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
@Entity()
export class Tag {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  tagName: string;

  @OneToMany(() => PostTag, (postTag) => postTag.tag)
  postTags: PostTag[];

  getPostTags(): PostTag[] {
    return this.postTags;
  }
}
