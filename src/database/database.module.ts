import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Tag } from 'src/tags/entities/tag.entity';
import { Post } from 'src/posts/entities/post.entity';
import { User } from 'src/users/entities/user.entity';
import { PostTag } from 'src/post-tags/entities/post-tag.entity';
import { ReportPost } from 'src/report-post/entities/report-post.entity';
@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('METAL_HOST'),
        port: configService.get('METAL_PORT'),
        username: configService.get('METAL_USERNAME'),
        password: configService.get('METAL_PASSWORD'),
        database: configService.get('METAL_NAMEDB'),
        entities: [Tag, Post, User, PostTag, ReportPost],
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
