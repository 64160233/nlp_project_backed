// src/report-post/report-post.service.ts
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateReportPostDto } from './dto/create-report-post.dto';
import { UpdateReportPostDto } from './dto/update-report-post.dto';
import { User } from 'src/users/entities/user.entity';
import { ReportPost } from './entities/report-post.entity';
import { Post } from '../posts/entities/post.entity';

@Injectable()
export class ReportPostService {
  constructor(
    @InjectRepository(ReportPost)
    private reportPostRepository: Repository<ReportPost>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {}

  async create(createReportPostDto: CreateReportPostDto): Promise<ReportPost> {
    console.log(createReportPostDto);
    const { user_id, post_id, reason } = createReportPostDto;

    const user = await this.userRepository.findOneBy({ id: user_id });
    const post = await this.postRepository.findOneBy({ id: post_id });

    if (!user || !post) {
      throw new NotFoundException('User or Post not found');
    }

    const reportPost = this.reportPostRepository.create({
      reportedBy: user,
      post,
      reason,
    });

    return this.reportPostRepository.save(reportPost);
  }

  async findAll(): Promise<ReportPost[]> {
    return this.reportPostRepository.find({
      relations: ['reportedBy', 'post'],
    });
  }

  async findOne(id: number): Promise<ReportPost> {
    const report = await this.reportPostRepository.findOne({
      where: { id },
      relations: ['reportedBy', 'post'],
    });

    if (!report) {
      throw new NotFoundException('Report not found');
    }
    return report;
  }

  async update(
    id: number,
    updateReportPostDto: UpdateReportPostDto,
  ): Promise<ReportPost> {
    await this.reportPostRepository.update(id, updateReportPostDto);
    const updatedReport = await this.findOne(id);
    return updatedReport;
  }

  async remove(id: number): Promise<void> {
    const result = await this.reportPostRepository.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException('Report not found');
    }
  }
}
