// src/report-post/dto/create-report-post.dto.ts
import { IsNotEmpty, IsOptional, IsBoolean, IsString } from 'class-validator';

export class CreateReportPostDto {
  @IsNotEmpty()
  reportedById: number;

  @IsNotEmpty()
  postId: number;

  @IsString()
  @IsOptional()
  reason?: string;

  user_id: number; // ID of the user reporting the post
  post_id: number;
}
