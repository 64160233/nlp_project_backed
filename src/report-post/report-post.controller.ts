// src/report-post/report-post.controller.ts
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { ReportPostService } from './report-post.service';
import { CreateReportPostDto } from './dto/create-report-post.dto';
import { UpdateReportPostDto } from './dto/update-report-post.dto';
import { ReportPost } from './entities/report-post.entity';

@Controller('report-post')
export class ReportPostController {
  constructor(private readonly reportPostService: ReportPostService) {}

  @Post()
  create(
    @Body() createReportPostDto: CreateReportPostDto,
  ): Promise<ReportPost> {
    return this.reportPostService.create(createReportPostDto);
  }

  @Get()
  findAll(): Promise<ReportPost[]> {
    return this.reportPostService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<ReportPost> {
    return this.reportPostService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: number,
    @Body() updateReportPostDto: UpdateReportPostDto,
  ): Promise<ReportPost> {
    return this.reportPostService.update(id, updateReportPostDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number): Promise<void> {
    return this.reportPostService.remove(id);
  }
}
