import { Post } from 'src/posts/entities/post.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm';

@Entity()
export class ReportPost {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.reportedPosts)
  reportedBy: User;

  @ManyToOne(() => Post, (post) => post.reports)
  post: Post;

  @Column({ type: 'text', nullable: true })
  reason: string;

  @Column({ type: 'boolean', default: false })
  reviewed: boolean;

  @Column({ type: 'boolean', default: false })
  inappropriate: boolean;

  @CreateDateColumn()
  reportedAt: Date;
}
