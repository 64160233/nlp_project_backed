// src/report-post/report-post.module.ts
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReportPostService } from './report-post.service';
import { ReportPostController } from './report-post.controller';
import { ReportPost } from './entities/report-post.entity';
import { User } from 'src/users/entities/user.entity';
import { Post } from 'src/posts/entities/post.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ReportPost, User, Post])],
  controllers: [ReportPostController],
  providers: [ReportPostService],
})
export class ReportPostModule {}
