import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TagsModule } from './tags/tags.module';
import { PostsModule } from './posts/posts.module';
import { UsersModule } from './users/users.module';
import { PostTagsModule } from './post-tags/post-tags.module';
import { AuthsModule } from './auths/auths.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './database/database.module';
import { ReportPostModule } from './report-post/report-post.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UsersModule,
    PostTagsModule,
    AuthsModule,
    TagsModule,
    PostsModule,
    DatabaseModule,
    ReportPostModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
